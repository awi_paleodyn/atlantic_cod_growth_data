# Atlantic cod growth data

The repository contains example weight-at-age datasets from Atlantic cod [growth model](https://gitlab.hzdr.de/awi_paleodyn/growth-model-atlantic-cod) outputs and [ICES](https://www.ices.dk/Science/publications/Pages/Scientific-reports.aspx) reports.

